import XCTest

import GeoJSONKitTests

var tests = [XCTestCaseEntry]()
tests += GeoJSONKitTests.allTests()
XCTMain(tests)
